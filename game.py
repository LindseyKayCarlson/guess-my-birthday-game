# Pseudocode
#
# print "Hi! What's your name?", and store answer as variable "name"
#
# Define "months" as a random integer between 0 and 13
# Define "years" as a random integer between 1923 and 2005
#
# print "Name, were you born in months, years?, and store answer as response
# if response is yes,
#   print "I knew it", and end game
# elif response is no, AND the guess is number 2, 3, or 4
#   print "Drat, let me try again"
#   game?
# else
#   print "I have other things to do. Good Bye" & exit

from random import randint

month = randint(1,12)
year = randint(1924,2004)

user_name = input("Hi! What's your name? ")

#Guess 1

print("Guess 1: ", user_name, ", were you born in",
        month, "/", year, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Let me try again!")

#Guess 2

month = randint(1,12)
year = randint(1924,2004)

print("Guess 2: ", user_name, ", were you born in",
        month, "/", year, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Let me try again!")

#Guess 3

month = randint(1,12)
year = randint(1924,2004)

print("Guess 3: ", user_name, ", were you born in",
        month, "/", year, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Let me try again!")

#Guess 4

month = randint(1,12)
year = randint(1924,2004)

print("Guess 4: ", user_name, ", were you born in",
        month, "/", year, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("Drat! Let me try again!")

#Guess 5

month = randint(1,12)
year = randint(1924,2004)

print("Guess 5: ", user_name, ", were you born in",
        month, "/", year, "?")

response = input("yes or no? ")

if response == "yes":
    print("I knew it!")
    exit()
else:
    print("I have other things to do. Good bye.")
    exit()
